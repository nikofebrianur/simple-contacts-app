import React from 'react'
import ContactItem from './NoteActiveItem'

function NoteActive({contacts, onDelete}) {
    return (
        <div className='contact-list'>
            {
                contacts.map((contact) => (
                    <ContactItem 
                    key={contact.id} 
                    id={contact.id}
                    onDelete={onDelete}
                    {...contact}/>
                ))
            }
        </div>
    )
}

export default NoteActive