import React from 'react';
 
function MoveButton({ id, onMove }) {
  return <button className='note-item__delete' onClick={() => onMove(id)}>^</button>
}
 
export default MoveButton;