import React from 'react'
import NoteArchiveItem from './NoteArchiveItem'

function NoteArchive({contacts, onDelete}) {
    return (
        <div className='contact-list'>
            {
                contacts.map((contact) => (
                    <NoteArchiveItem 
                    key={contact.id} 
                    id={contact.id}
                    onDelete={onDelete}
                    {...contact}/>
                ))
            }
        </div>
    )
}

export default NoteArchive