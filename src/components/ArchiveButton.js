import React from 'react';
 
function ArchiveButton({ id, onArchive }) {
  return <button className='note-item__delete' onClick={() => onArchive(id)}>%</button>
}
 
export default ArchiveButton;