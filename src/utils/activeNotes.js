const activeNotes = () => {
    return [
        {
            id: 1,
            name: 'Pikachu',
            tag: 'pikachu',
            imageUrl: '/images/pikachu.png'
        },
        {
            id: 2,
            name: 'Bulbasaur',
            tag: 'bulbasaur',
            imageUrl: '/images/bulbasaur.png'
        },
        {
            id: 3,
            name: 'Charmander',
            tag: 'charmander',
            imageUrl: '/images/charmander.png'
        },
    ]
}

export { activeNotes };