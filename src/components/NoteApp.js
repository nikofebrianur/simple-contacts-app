import React from 'react'
import { activeNotes } from '../utils/activeNotes'
import { archiveNotes } from '../utils/archiveNotes'
import NoteInput from './NoteInput';
import NoteActive from './NoteActive';
import NoteArchive from './NoteArchive';

class NoteApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contacts: activeNotes(),
            archiveNotes: archiveNotes()
        }

        this.onDeleteHandler = this.onDeleteHandler.bind(this);
        this.onArchiveHandler = this.onArchiveHandler.bind(this);
        this.onAddContactHandler = this.onAddContactHandler.bind(this);
    }

    onDeleteHandler(id) {
        const contacts = this.state.contacts.filter(contact => contact.id !== id);
        this.setState({ contacts });
    }

    onArchiveHandler(id) {
        const contacts = this.state.contacts.filter(contact => contact.id !== id);
        this.setState({ contacts });
    }

    onAddContactHandler({ name, tag }) {
        this.setState((prevState) => {
            return {
                contacts: [
                    ...prevState.contacts,
                    {
                        id: +new Date(),
                        name,
                        tag,
                    }
                ]
            }
        })
    }

    render() {
        return (
            <div className='contact-app'>
                <h1>My Daily Notes</h1>
                <h2>Add Note</h2>
                <NoteInput addContact={this.onAddContactHandler}/>
                <h2>Active Notes</h2>
                <NoteActive
                    contacts={this.state.contacts}
                    onArchive={this.onArchiveHandler}
                    onDelete={this.onDeleteHandler}
                />
                <h2>Archive Notes</h2>
                <NoteArchive
                    contacts={this.state.contacts}
                    onMove={this.onMoveHandler}
                    onDelete={this.onDeleteHandler}
                />
            </div>
        )
    }
}

export default NoteApp