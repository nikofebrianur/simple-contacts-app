import React from 'react'
import NoteItemBody from './NoteItemBody'
import MoveButton from './MoveButton';
import DeleteButton from './DeleteButton';

function NoteArchiveItem({ name, tag, id, onMove, onDelete }) {
    return (
        <div className='note-item'>
            <NoteItemBody name={name} tag={tag}/>
            <MoveButton id={id} onMove={onMove} />
            <DeleteButton id={id} onDelete={onDelete} />
        </div>
    )
}

export default NoteArchiveItem