import React from 'react'
import NoteItemBody from './NoteItemBody'
import ArchiveButton from './ArchiveButton';
import DeleteButton from './DeleteButton';

function NoteActiveItem({ name, tag, id, onArchive, onDelete }) {
    return (
        <div className='note-item'>
            <NoteItemBody name={name} tag={tag}/>
            <ArchiveButton id={id} onArchive={onArchive} />
            <DeleteButton id={id} onDelete={onDelete} />
        </div>
    )
}

export default NoteActiveItem